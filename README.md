<img src="src/public/img/favicon.png" width="100px" height="100px" style="margin: 0 0 15px 0"/>

# bologhu

**bologhu** merupakan blog pribadi yang berisikan posting tentang pemrograman

Backend dikembangkan dengan menggunakan teknologi berikut:

1. [NodeJS](https://nodejs.dev/)
2. [ExpressJS](https://expressjs.com/)
3. [Sequelize](https://sequelize.org/)
4. [Handlebars](https://handlebarsjs.com/)

Frontend dikembangkan dengan menggunakan teknologi berikut:

1. [Bootstrap 4.6](https://getbootstrap.com/docs/4.6/getting-started/introduction/)
2. [Fontello](https://fontello.com/)
3. [Iconmonstr](https://iconmonstr.com/)
4. [Marked](https://marked.js.org/)