require('module-alias/register');

const cacheService = require('@services/cache-service');
const clientService = require('@services/client-service');
const RANDOM = 'random';


async function findAndCountAll(req) {
  let url = req.params.tag ? `/api/posts/tags/${req.params.tag}` : `/api/posts`;
  let { keyword, page } = req.query;

  req.client = {
    params : {}
  };

  if (keyword) {
    req.client.params = { title: keyword, description: keyword, op: 'or' }
  }

  if (page) {
    req.client.params.page = page
  }

  let response = await clientService.get(req, url);

  if (keyword) {
    if (response.data.pagination.hasPrevious) {
      response.data.pagination.previousUrl = `?page=${page - 1}&keyword=${keyword}`
    }

    if (response.data.pagination.hasNext) {
      response.data.pagination.nextUrl = `?page=${page + 1}&keyword=${keyword}`
    }
  }

  return response;
}

async function findRandom(req) {
  if (cacheService.cache.has(RANDOM)) {
    return cacheService.cache.get(RANDOM);
  } else {

    let response = await clientService.get(req, `/api/posts/random?tags=nodejs,java`);
    cacheService.set(RANDOM, response.data);

    return response.data;
  }
}


async function viewPost(req) {
  return await clientService.get(req, `/api/posts/view/${req.params.code}`);
}

module.exports = {
  findAndCountAll,
  findRandom,
  viewPost
};
