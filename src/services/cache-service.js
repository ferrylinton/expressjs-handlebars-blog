require('module-alias/register');

const NodeCache = require('node-cache');
const logger = require('@configs/logger');


const cache = new NodeCache({ stdTTL: 100, checkperiod: 120 });

function set(key, value){
    let success = cache.set(key, value, 300);

    if (success) {
      logger.info('set cache for ' + key);
    } else {
      logger.info('failed to set cache for ' + key);
    }
}

module.exports = {
    set,
    cache
};
