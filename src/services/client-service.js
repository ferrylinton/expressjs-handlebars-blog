require('module-alias/register');

const axios = require('axios');
const rax = require('retry-axios');
const requestIp = require('request-ip');
const logger = require('@configs/logger');
const retry = parseInt(process.env.AXIOS_RETRY || '3', 10);

const axiosInstance = axios.create({
    baseURL: process.env.BLOG_API_BASE_URL,
    timeout: process.env.AXIOS_TIMEOUT
});

axiosInstance.defaults.raxConfig = {
    instance: axiosInstance,
    retry,
    onRetryAttempt: err => {
        logger.error(`calling ${err.config.baseURL}${err.config.baseURL} timeout after ${err.config.timeout}ms`);
        const cfg = rax.getConfig(err);
        logger.error(`Retry attempt #${cfg.currentRetryAttempt}`);
    }
};

async function get(req, url) {
    rax.attach(axiosInstance);
    let response = await axiosInstance(config(req, url));
    return response;
}

function config(req, url) {
    let config = {
        url,
        headers: {
            'user-agent': req.headers['user-agent'],
            'x-forwarded-for': requestIp.getClientIp(req)
        }
    }

    if (req.client.params) {
        config.params = req.client.params
    }

    if (req.client.responseType) {
        config.responseType = req.client.responseType;
    }

    return config;
}

module.exports = {
    get
};
