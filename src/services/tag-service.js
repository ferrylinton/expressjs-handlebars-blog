require('module-alias/register');

const cacheService = require('@services/cache-service');
const clientService = require('@services/client-service');
const TAGS = 'tags';


async function findAll(req) {
  if (cacheService.cache.has(TAGS)) {
    return cacheService.cache.get(TAGS);
  } else {

    let response = await clientService.get(req, '/api/tags/all');
    let tags = response.data.map(function (item) {
      return item['name'];
    });

    cacheService.set(TAGS, tags);
    return tags;
  }
}

module.exports = {
  findAll
};
