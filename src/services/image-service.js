require('module-alias/register');

const clientService = require('@services/client-service');


async function viewImage(req) {
  req.client = {
    responseType : 'arraybuffer'
  }

  return await await clientService.get(req, `/api/images/view/${req.params.name}`);
}

module.exports = {
  viewImage
};
