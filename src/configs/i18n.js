const i18n = require('i18n');
const path = require('path');

i18n.configure({
    locales: ['id'],
    defaultLocale: 'id',

    // change 'accept-language' because browser always send 'accept-language'
    // Accept-Language: en-US,en;q=0.9 , depend on your browser setting
    header: 'x-accept-language', 

    directory: path.join(process.cwd(), 'src', 'locales'),
    autoReload: true,
    updateFiles: false,
    syncFiles: false,
});

module.exports = function (app) {

    // default: using 'accept-language' header to guess language settings
    app.use(i18n.init);
    
};