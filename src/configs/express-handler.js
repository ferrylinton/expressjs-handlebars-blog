require('module-alias/register');

const createError = require('http-errors');
const logger = require('@configs/logger');


module.exports = function (app) {

    app.use(function (req, res, next) {
        next(createError(404));
    });

    app.use(function (err, req, res, next) {
        logger.error((err) ? err.stack : err);

        if(err.name === 'NotFoundError'){
            res.locals.status = 404;
            res.locals.message = res.__('404');
        }else if(err.code === 'ECONNREFUSED'){
            res.locals.status = 500;
            res.locals.message = res.__('ECONNREFUSED', err.config.url);
        }else if(err.response && err.response.status == 404){
            res.locals.status = 404;
            res.locals.message = res.__('dataIsNotFound');
        }else{
            res.locals.status = (err.response && err.response.status) ? err.response.status : 500;
            res.locals.message = (err.config && err.config.timeoutErrorMessage) ? err.config.timeoutErrorMessage : err.message;
        }

        res.render('error');
    });

};
