require('module-alias/register');
const Handlebars = require("handlebars");


const list = {

    link: function () {
        arguments = [...arguments].slice(0, -1);
        url = arguments.join('');
        url = Handlebars.escapeExpression(url);
        return new Handlebars.SafeString(url);
    },

    equals: function (string1, string2) {
        return string1 === string2
    },

    checkedFirst: function (val) {
        if(val === 0){
            return 'checked=checked';
        }else{
            return '';
        }
        
    },

    concat: function () {
        var result = '';

        for (var i in arguments) {
            result += (typeof arguments[i] === 'string' ? arguments[i] : '') + '';
        }

        return result.trim();
    },

    addClass: function (condition, val1, val2) {
        if (condition) {
            return val1;
        } else {
            return val2;
        }
    },

    toArray: function (varValue) {
        if (varValue) {
            return varValue.split(',');
        } else {
            return [];
        }
    },

    isSelected: function (path, options) {
        if (options.data.root.path === path) {
            return 'selected';
        } else if (options.data.root.path === path) {
            return 'selected';
        } else {
            return '';
        }
    },

    trim: function (str) {
        return typeof str === 'string' ? str.trim() : '';
    },

    env: function (name) {
        return process.env[name]
    },

    cdn: function () {
        return (process.env.CDN && process.env.CDN === 'true')
    },

    inc: function (value) {
        return parseInt(value) + 1;
    },

    isEmptyArray: function (val) {
        return val.length === 0
    }

}

module.exports = {
    list: list
};