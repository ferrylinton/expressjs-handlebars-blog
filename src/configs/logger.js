require('module-alias/register');

const path = require('path');
const { createLogger, format, transports } = require('winston');
const { combine, splat, timestamp, printf } = format;
const env = process.env.NODE_ENV || 'development';

const customFormat = printf(({ level, message, timestamp, ...metadata }) => {
    if (metadata && metadata.file) {
        return `${timestamp}[${level}][${metadata.file}] ${message} `;
    } else {
        return `${timestamp}[${level}] ${message} `;
    }
});

const logger = createLogger({
    transports: [
        new transports.File({
            level: 'info',
            filename: path.join(process.cwd(), 'logs', 'app.log'),
            handleExceptions: true,
            json: false,
            maxsize: 5242880, // 5MB
            maxFiles: 5,
            colorize: false,
            format: combine(
                splat(),
                timestamp({
                    format: 'MM/DD-HH:mm:ss'
                }),
                customFormat
            )
        }),

    ],
    exitOnError: false
});

if (env !== 'production') {
    logger.add(new transports.Console({
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
        format: combine(
            format.colorize(),
            splat(),
            timestamp({
                format: 'MM/DD-HH:mm:ss'
            }),
            customFormat
        )
    }));
}

logger.stream = {
    write: function (message, encoding) {
        logger.info(message);
    }
};

module.exports = logger;