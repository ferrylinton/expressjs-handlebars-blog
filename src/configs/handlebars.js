require('module-alias/register');

const path = require('path');
const exphbs = require('express-handlebars');
const handlebarsHelpers = require('@configs/handlebars-helpers');

const hbs = exphbs.create({
    layoutsDir: path.join(process.cwd(), 'src', 'views', 'layouts'),
    partialsDir: path.join(process.cwd(), 'src', 'views', 'partials'),
    defaultLayout: 'main',
    extname: '.hbs',
    helpers: handlebarsHelpers.list
});

module.exports = function (app) {
    app.engine('.hbs', hbs.engine);
    app.set('views', path.join(process.cwd(), 'src', 'views'));
    app.set('view engine', 'hbs');
    
    app.use(function (req, res, next) {
        if (app.locals && req) {
            app.locals.path = req.path;

            if(req.path.includes('tags')){
                app.locals.searchPath = req.path;
            }else{
                app.locals.searchPath = '/';
            }
        }

        next();
    });
};
