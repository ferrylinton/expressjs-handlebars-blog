require('module-alias/register');

const logger = require('@configs/logger');
const tagService = require('@services/tag-service');
const postService = require('@services/post-service');

module.exports = function (app) {

    app.use(async (req, res, next) => {
        try {
            res.locals.tags = await tagService.findAll(req);
            res.locals.random = await postService.findRandom(req);
        } catch (error) {
            logger.error((error) ? error.stack : error);
        }

        next();
    });

};
