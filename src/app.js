require('module-alias/register');

const path = require('path');
const express = require('express');
const i18n = require('@configs/i18n');
const handlebars = require('@configs/handlebars');
const cacheData = require('@configs/cache-data');
const expressHandler = require('@configs/express-handler');
const routes = require('@routes/index');
const favicon = require('serve-favicon')


var app = express();

app.use(favicon(path.join(process.cwd(), 'src', 'public', 'img', 'favicon.png')))
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(process.cwd(), 'src', 'public')));

// Config Before Request
handlebars(app);
i18n(app);
cacheData(app);

// Add Routes
routes(app);

// Config For Response
expressHandler(app);

module.exports = app;
