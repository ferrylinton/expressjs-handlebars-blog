window.onload = function () {
    let disableds = document.querySelectorAll('.disabled');

    for (let i = 0; i < disableds.length; i++) {
        disableds[i].onclick = function () { return false; };
    }

    let links = document.querySelectorAll('.markdown-body a');

    for (let i = 0; i < links.length; i++) {
        links[i].setAttribute('target', '_blank');
    }
};

function openSidebar() {
    document.getElementById('sidebar').style.left = 0;
    document.getElementById('overlay').style.display = 'block';
}

function closeSidebar() {
    document.getElementById('sidebar').style.left = '-250px';
    document.getElementById('overlay').style.display = 'none';
}