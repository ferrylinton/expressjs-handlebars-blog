require('module-alias/register');

const express = require('express');
const marked = require('marked');
const lodash = require('lodash');
const postService = require('@services/post-service');
const imageService = require('@services/image-service');
const cacheService = require('@services/cache-service');

var router = express.Router();
router.get('/', posts);
router.get('/posts/tags/:tag', posts);
router.get('/posts/:code', viewPost);
router.get('/posts/images/:name', viewImage);
router.get('/author', author);
router.get('/cache', cache);

async function posts(req, res, next) {
  try {
    let response = await postService.findAndCountAll(req);
    let data = {};

    if (response.status) {
      data = response.data;
    } else {
      if (response.code === 'ECONNREFUSED') {
        data.errorMessage = `Tidak bisa melakukan koneksi ke ${response.config.url}`;
      }
    }
    
    res.render('posts', { ...data, ...req.query });
  } catch (error) {

    if(error.code === 'ECONNABORTED'){
      res.render('posts', { error: true, url : req.originalUrl });
    }else{
      return next(error);
    }

  }
}

async function viewPost(req, res, next) {
  try {
    let response = await postService.viewPost(req);
    let post = response.data;

    if (post) {
      if (req.get('host').includes('localhost')) {
        post.content = marked(post.content.toString().replace('bologhu.com', req.get('host')));
      } else {
        post.content = marked(post.content.toString());
      }
    }

    res.render('post', post);
  } catch (error) {
    if(error.code === 'ECONNABORTED'){
      res.render('posts', { error: true, url : req.originalUrl });
    }else{
      return next(error);
    }
  }
}

async function viewImage(req, res, next) {
  try {
    let response = await imageService.viewImage(req);
    let buffer = Buffer.from(response.data, 'base64');

    res.writeHead(200, {
      'Content-Type': response.headers['content-type'],
      'Content-Length': response.headers['content-length']
    });
    res.end(buffer);
  } catch (error) {
    return next(error);
  }
}

async function author(req, res, next) {
  try {
    res.render('author');
  } catch (error) {
    return next(error);
  }
}

function cache(req, res, next) {
  try {
    let result = {
      operation: req.query.operation
    }

    if (result.operation === 'del' && req.query.key) {
      result.data = {};
      result.data.key = req.query.key;
      result.data.del = cacheService.cache.del(result.data.key);
    } else if (result.operation === 'keys') {
      result.data = getAllValues();
    } else if (result.operation === 'stats') {
      result.data = cacheService.cache.getStats();
    } else if (result.operation === 'flushAll') {
      cacheService.cache.flushAll();
      result.data = cacheService.cache.getStats();
    }

    res.render('cache', result);
  } catch (error) {
    return next(error)
  }
}

function getAllValues() {
  let data = {};
  let keys = cacheService.cache.keys();
  
  keys.forEach(key => {
    data[key] = cacheService.cache.get(key);

    if(lodash.isObject(data[key])){
      data[key] = JSON.stringify(data[key]);
    }
  });

  return data;
}

module.exports = router;
