require('module-alias/register');

const indexRoute = require('@routes/index-route');

module.exports = function (app) {

    app.use('/', indexRoute);

};
