require('module-alias/register');
require('dotenv').config();

const logger = require('@configs/logger');
const port = parseInt(process.env.PORT || '3000', 10);


var app = require('@root/app');

app.listen(port, function () {
    logger.info(`####################################################################`)
    logger.info(`NODE_ENV   : ${process.env.NODE_ENV}`);
    logger.info(`address    : ${JSON.stringify(this.address())}`);
    logger.info(`####################################################################`)
});